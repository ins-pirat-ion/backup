Boomerang
---------
* kombo tweed + reverb
* guitar both
* clean / octaver:  fat - sub 1 - dry 9 - up 8

Acid?
-----
* kombo tweed + reverb
* guitar high
* C9 press tone: mod 0 - click max

Say no to sex
-------------
* kombo tweed + reverb
* guitar high
* octaver: swell - sub 12, dry 12, up 0

Intro
-----
* kombo tweed + reverb
* guitar high
* octaver: swell - sub 9, dry 12, up 0 / C9 Lord Purple mod 12, click 12

Just a click away
-----------------
* kombo UK 70's + reverb
* guitar high - volume 4 / 10
* octaver: tight - sub 9, dry 12, up 0

Man on the bench
----------------
* kombo UK 70's + reverb
* guitar both - volume 3 / 10

Lost in time
------------
* kombo tweed + reverb
* guitar high
* octaver: swell - sub 9, dry 12, up 0

Little girl
-----------
* kombo UK 70's + reverb
* guitar high
* octaver: swell - sub 9, dry 12, up 0

Singing guitar
-------------
* kombo UK 70's + reverb
* guitar high
* C9: mello flutes dry 10, organ 12, nod 12, click 12
