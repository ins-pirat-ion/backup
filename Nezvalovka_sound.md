Píseň
-----
* kombo tweed + reverb
* guitar bridge
* C9: mello flutes dry 10, organ 12, nod 12, click 12

Mrtvý vrabec
------------
* kombo UK 70's + reverb
* guitar both - volume 3 / 10 ?
* octaver: fat - sub 9, dry 12, up 0

Sedávám
--
* kombo UK 70's + reverb
* guitar both - volume 3 / 10 ?
* octaver: swell - sub 10, dry 12, up 0


Imperial blues
--
* kombo tweed + reverb
* guitar bridge

Pocoucovské štíty
--
* kombo tweed + reverb
* guitar bridge
* C9: blimp  dry 12, organ 12, mod max, click 0


--
